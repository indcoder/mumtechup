Template.welcome.helpers({
  'getWelcomeMessage' : function(name ){
    console.log("Name in welcome message " +  name);
    return pickOne([" Welcome to the Docker Birthday", " is in Da house", ", thanks for attending #dockerbirthday" , ", great to have you with us" ,

      ", glad that you could make it"]);
  },

  'getNewAttendee' : function(){
    return RSVPS.findOne({displayed : { $not : 'y'} });
  },

  'wasWelcomed' : function(_id){
    console.log('Was welcomed');
    Meteor.setTimeout(function(){
      RSVPS.update({'_id':_id}, {$set :{'displayed' : 'y'}});
      Meteor.call('getEventRSVPs',  Session.get('meetup_key'), Session.get('event_id'));
    }, 50000);


  }

});

var pickOne = function(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
};

Meteor.subscribe('RSVPS');
