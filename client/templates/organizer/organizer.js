Template.organizer.events({
  'click .submit' : function(){
    console.log('Submit organizer details clicked, event id: ' + $('#event_id').val() ) ;
    console.log('User token logged in ' + Meteor.user().services.meetup.accessToken);

    //Session.set('meetup_key', $('#meetup_key').val());
    Meteor.call('getEventRSVPs', $('#event_id').val(), Meteor.user().services.meetup.accessToken, function(error, result){
      if (error){
        sAlert.error(error);
        console.log("Error thrown on client " + error);
      }

      else if (result){
        console.log("Successful call " + result);
        sAlert.success("Successfully called Meetup API");
        Session.set('event_id', $('#event_id').val());
        Router.go('/welcome/');
      }

    });


  }

});
