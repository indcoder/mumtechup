Template.challenge.helpers({
  settings: function() {
    return {
      position: Session.get("position"),
      limit: 5,
      rules: [
        {
          collection: RSVPS,
          field: "name",
          matchAll : true,
          template : Template.attendeeName
        }
      ]
    };
  }
});

Template.challenge.events({
  'click .btn-primary': function(){
    console.log('Attendee Name ' + selectedDoc._id);
  },

  "autocompleteselect input": function(event, template, doc) {
    console.log("selected ", doc);
    selectedDoc = doc;
 }
});

var selectedDoc;
