Meteor.methods({
  'getEventRSVPs': function (event_id, accessToken, call_error, result) {
    // avoid blocking other method calls from the same client
    this.unblock();
    console.log('event_id received from client ' + event_id);

    // asynchronous call to the API calling function
    //var response = Meteor.wrapAsync(apiCall)(event_id);
    //return response;
    var response = Meteor.wrapAsync(apiCall);
    return response(event_id, accessToken);
  }
});



var apiCall = function(event_id,accessToken,callback){
  var apiUrl = 'https://api.meetup.com/2/rsvps?access_token=' + accessToken + '&event_id=' + event_id +'&fields=attendance_status';
  console.log('ApiURL ' + apiUrl);
  HTTP.get(apiUrl, function( error , response ) {
    if ( error ) {
      if(error.response){
        var errorCode = error.response.data.code;
        var errorMessage = error.response.data.message;
      } else {
        var errorCode = 500;
        var errorMessage = "Error in invoking the API" ;
      }

      console.error(error);
      var call_error = new Meteor.Error(errorCode, errorMessage);
      callback(call_error, null);

      //throw myError;
      //return throwError(' not_authorized', 'The credentials you provided do not have the requisite rights to perform this operation');
    } else {
        console.log( response.data.results.length );
        _.each(response.data.results, function(item){
            if( item.attendance_status === 'attended'){
              console.log('Member _Id ' + item.member.member_id + ' event_id '+ event_id );
              //Since Meteor lacks the facility to prevent duplicate inserts[no composite primary key + limitations for indexes on minimongo],
              // we need to ascertain that identical combination of event and member is not pre existiing before we insert
              count = RSVPS.find({member_id: item.member.member_id , event_id : event_id}).count();
              if (count === 0){
                var doc = {
                  member_id : item.member.member_id  ,
                  name : item.member.name,
                  event_id : event_id ,
                  attendance_status : item.attendance_status
                }

                if (item.member_photo && item.member_photo.photo_link)
                {
                  doc.photo_link = item.member_photo.photo_link ;
                }

                else
                {
                  doc.photo_link = '/images/birthday.png'
                }

                console.log(doc);
                RSVPS.insert(doc);
            }
          }
        });

        callback(null, true);
    }
  });
};


if (Meteor.isServer) {
  Meteor.publish("RSVPS", function () {
    return RSVPS.find({});
  });

  RSVPS.allow({
    update : function(){
      return true;
    }
  });
}
