
isKnownError = function (error) {
  var errorName = error && error.error;
  var listOfKnownErrors = ['no-project-name', 'user-not-logged-in', 'project-not-found', 'not_authorized'];

  return _.contains(listOfKnownErrors, errorName);
};


throwError = function(error, reason, details) {
  var meteorError = new Meteor.Error(error, reason, details);

  if (Meteor.isClient) {
    // this error is never used
    // on the client, the return value of a stub is ignored
    return meteorError;
  }

  if (Meteor.isServer) {
    throw meteorError;
  }
};
