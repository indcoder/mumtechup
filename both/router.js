Router.configure({ layoutTemplate: 'layout' });

Router.map(function(){

  this.route('organizer', {
    path: '/',
    template:'organizer'
  });

  this.route('welcome', {
    path: '/welcome',
    template:'welcome'
  });

  this.route('challenge', {
    path: '/challenge',
    template:'challenge'
  });

});
